package com.huawei.timeclaims.timeclaims.model;


import com.google.gson.Gson;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Table(name = "claims")
public class Claim extends AuditModel {

    @Id
    @GeneratedValue
    protected Long id;

    protected int week; //Work Week

    protected String date; //Claim Period

    protected String approvedDate; //Approved Date

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "members_id", nullable = false)
    protected Member member; //Claimer Employee ID + Claimer + Claimer Dept Code + Claimer Dept

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "projects_id", nullable = false)
    protected Project project; //Project Name + Project Code

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "status_id", nullable = false)
    protected Status status;


    public Claim() {
    }

    public Claim(int week, String date, Member member, Project project, Status status) {
        this.date = date;
        this.week = week;
        this.member = member;
        this.project = project;
        this.status = status;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(String approvedDate) {
        this.approvedDate = approvedDate;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ClaimLogs toClaimLogs(String importStatus){
        return new ClaimLogs(this.week,this.date,this.member,this.project,this.status,importStatus);
    }
}