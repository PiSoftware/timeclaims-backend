package com.huawei.timeclaims.timeclaims.model;


import com.google.gson.Gson;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "projects")
public class Project {
    @Id @GeneratedValue
    private Long id;

    @NotNull @Size(min =2)
    private String siteName;
    private String projectCode;
    private String subProjectCode;
    private String projectType;
    private String approver;
    private String projectLevel;


    private String projectName;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "members_id", nullable = false)
    private Member tracker;

    @Column(name="is_active")
    private Boolean active=true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getSubProjectCode() {
        return subProjectCode;
    }

    public void setSubProjectCode(String subProjectCode) {
        this.subProjectCode = subProjectCode;
    }

    public String getProjectType() {
        return projectType;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public String getApprover() {
        return approver;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }

    public String getProjectLevel() {
        return projectLevel;
    }

    public void setProjectLevel(String projectLevel) {
        this.projectLevel = projectLevel;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Member getTracker() {
        return tracker;
    }

    public void setTracker(Member tracker) {
        this.tracker = tracker;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}