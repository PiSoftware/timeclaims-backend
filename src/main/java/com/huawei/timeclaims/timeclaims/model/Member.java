package com.huawei.timeclaims.timeclaims.model;

import com.google.gson.Gson;
import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "members")
public class Member {

    @Id
    private Long id;

    @NotNull
    private String fullName;

    @NotNull
    @Column(columnDefinition = "text")
    private String team;

    @Column(name="is_active")
    private Boolean active=true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}