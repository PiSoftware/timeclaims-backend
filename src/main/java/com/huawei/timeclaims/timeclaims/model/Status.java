package com.huawei.timeclaims.timeclaims.model;


import com.google.gson.Gson;
import lombok.Data;
import javax.persistence.*;

@Entity
@Table(name = "status")
public class Status extends AuditModel {
    @Id @GeneratedValue
    private Long id;
    private double phm;
    private int pmd;
    private double amh;
    private String transferBy;
    private String status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "members_id", nullable = false)
    private Member tracker;

    private String remark;

    public Status (double phm, int pmd, double amh, String transferBy, String status,  String remark,Member tracker){
        this.phm = phm;
        this.pmd = pmd;
        this.amh = amh;
        this.transferBy = transferBy;
        this.status = status;
        this.tracker = tracker;
        this.remark = remark;
    }
    public Status (double phm, int pmd, double amh, String transferBy, String status,String remark){
        this.phm = phm;
        this.pmd = pmd;
        this.amh = amh;
        this.transferBy = transferBy;
        this.status = status;
        this.remark = remark;
    }
    public Status(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getPhm() {
        return phm;
    }

    public void setPhm(double phm) {
        this.phm = phm;
    }

    public int getPmd() {
        return pmd;
    }

    public void setPmd(int pmd) {
        this.pmd = pmd;
    }

    public double getAmh() {
        return amh;
    }

    public void setAmh(double amh) {
        this.amh = amh;
    }

    public String getTransferBy() {
        return transferBy;
    }

    public void setTransferBy(String transferBy) {
        this.transferBy = transferBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Member getTracker() {
        return tracker;
    }

    public void setTracker(Member tracker) {
        this.tracker = tracker;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}