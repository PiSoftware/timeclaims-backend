package com.huawei.timeclaims.timeclaims.model;

import com.google.gson.Gson;

import javax.persistence.*;

@Entity
@Table(name = "claimlogs")
public class ClaimLogs {

    public static final String NEW="NEW";
    public static final String UPDATE_AMH="UPDATE_AMH";
    public static final String SET_APPROVED="SET_APPROVED";

    public String getImportStatus() {
        return importStatus;
    }

    public void setImportStatus(String importStatus) {
        this.importStatus = importStatus;
    }

    String importStatus;

    @Id
    @GeneratedValue
    protected Long id;

    protected int week; //Work Week

    protected String date; //Claim Period

    protected String approvedDate; //Approved Date

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "members_id", nullable = false)
    protected Member member; //Claimer Employee ID + Claimer + Claimer Dept Code + Claimer Dept

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "projects_id", nullable = false)
    protected Project project; //Project Name + Project Code

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "status_id", nullable = false)
    protected Status status;


    public ClaimLogs() {
    }

    public ClaimLogs(int week, String date, Member member, Project project, Status status) {
        this.date = date;
        this.week = week;
        this.member = member;
        this.project = project;
        this.status = status;
    }
    public ClaimLogs(int week, String date, Member member, Project project, Status status,String importStatus) {
        this.date = date;
        this.week = week;
        this.member = member;
        this.project = project;
        this.status = status;
        this.importStatus=importStatus;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(String approvedDate) {
        this.approvedDate = approvedDate;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}