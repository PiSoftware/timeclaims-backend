package com.huawei.timeclaims.timeclaims;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
@EnableConfigurationProperties({
        FileStorageProperties.class
})
public class TimeclaimsApplication {
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(TimeclaimsApplication.class);
    }
    public static void main(String[] args) {
        SpringApplication.run(TimeclaimsApplication.class, args);
    }
}
