package com.huawei.timeclaims.timeclaims.repository;

import com.huawei.timeclaims.timeclaims.model.ClaimLogs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClaimLogsRepository extends JpaRepository<ClaimLogs, Long> {
    //List<Claim> findById(Long claimId);
    public List<ClaimLogs> findAllByOrderByIdDesc();

    @Query(value = "SELECT (CASE WHEN (COUNT(*)>0) THEN TRUE ELSE FALSE END) AS exists FROM claims LEFT JOIN projects ON projects.id = claims.projects_id  WHERE claims.week=:week AND claims.members_id = :memberId AND projects.project_code=:projectCode ", nativeQuery = true)
    public boolean checkExistClaim(@Param("week") Integer week, @Param("memberId") Integer memberId, @Param("projectCode") String projectCode);

    @Query(value = "DELETE * FROM claimlogs", nativeQuery = true)
    public boolean deleteAllLogs();

}
