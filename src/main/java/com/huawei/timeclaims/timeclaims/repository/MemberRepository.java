package com.huawei.timeclaims.timeclaims.repository;


import com.huawei.timeclaims.timeclaims.model.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface MemberRepository extends JpaRepository<Member, Long>  {
    //List<Member> findByClaimId(Long questionId);
    List<Member> findAllByActiveOrderByFullName(Boolean isActive);
}