package com.huawei.timeclaims.timeclaims.repository;

import com.huawei.timeclaims.timeclaims.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusRepository extends JpaRepository<Status, Long> {
}