package com.huawei.timeclaims.timeclaims.repository;


import com.huawei.timeclaims.timeclaims.model.Claim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClaimRepository extends JpaRepository<Claim, Long> {
    //List<Claim> findById(Long claimId);
    public List<Claim> findAllByOrderByWeekDesc();

    @Query(value = "SELECT claims.week, members.full_name FROM claims  inner join members on claims.members_id = members.id ",
            nativeQuery = true
    )
    public List<?> test();

    @Query(value = "SELECT (CASE WHEN (COUNT(*)>0) THEN TRUE ELSE FALSE END) AS exists FROM claims, projects WHERE claims.week=:week AND claims.members_id = :memberId AND projects.project_code=:projectCode ", nativeQuery = true)
    public boolean checkExistClaim(@Param("week") Integer week, @Param("memberId") Integer memberId, @Param("projectCode") String projectCode);

}