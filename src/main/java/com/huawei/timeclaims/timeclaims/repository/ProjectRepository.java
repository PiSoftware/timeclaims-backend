package com.huawei.timeclaims.timeclaims.repository;


import com.huawei.timeclaims.timeclaims.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

    //public List<Project> findAllByActiveOrderByProjectName(Boolean isActive);


    //public List<Project> findAllByProjectName();
    public Project findByProjectName(String projectName);
    //TODO : findByProjectCode

    @Query(value = "SELECT * FROM projects WHERE is_active = true ORDER BY site_name",
            nativeQuery=true
    )
    public List<Project> findProjectsQuery();

    @Query(value = "Select Distinct site_name from projects",
            nativeQuery=true
    )
    public List<Project> findDistinctProjectNames();
}