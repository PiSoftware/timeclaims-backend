package com.huawei.timeclaims.timeclaims.controller;


import com.huawei.timeclaims.timeclaims.model.Status;
import com.huawei.timeclaims.timeclaims.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin()
@RestController
public class StatusController {

    private StatusRepository statusRepository;

    @Autowired
    public StatusController(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @GetMapping("/status")
    public Page<Status> getStatus(Pageable pageable) {
        return statusRepository.findAll(pageable);
    }

    @PostMapping("/status")
    public Status createStatus(@Valid @RequestBody Status status) {
        return statusRepository.save(status);
    }

}