package com.huawei.timeclaims.timeclaims.controller;


import com.huawei.timeclaims.timeclaims.model.Claim;
import com.huawei.timeclaims.timeclaims.service.FileStorageService;
import com.huawei.timeclaims.timeclaims.utils.CustomJSON;
import com.huawei.timeclaims.timeclaims.utils.ExcelOperations;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@CrossOrigin()
@RestController
public class ExcelController {


    private FileStorageService fileStorageService;
    private ExcelOperations excelOperations;

    @Autowired
    public ExcelController(ExcelOperations excelOperations, FileStorageService fileStorageService) {
        this.excelOperations = excelOperations;
        this.fileStorageService = fileStorageService;
    }
    @GetMapping(value =
            "/export")
    public void exportFile(HttpServletRequest request, HttpServletResponse response) {
        excelOperations.exportExcel("TimeClaims.xlsx", response);
    }


    @PostMapping(value = "/import")
    public ResponseEntity importFile(@RequestParam("file") MultipartFile file) {

        String fileName = fileStorageService.storeFile(file);

        try {
            excelOperations.importExcel(fileName);
            return new ResponseEntity<>(CustomJSON.response("success","Imported", "Successfully Imported"), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(CustomJSON.response("warning","Warning", e.getMessage()), HttpStatus.OK);
        }

    }

    @GetMapping("/downloadFile/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            System.out.println("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

}