package com.huawei.timeclaims.timeclaims.controller;

import com.huawei.timeclaims.timeclaims.exception.ResourceNotFoundException;
import com.huawei.timeclaims.timeclaims.model.Member;
import com.huawei.timeclaims.timeclaims.model.Project;
import com.huawei.timeclaims.timeclaims.repository.MemberRepository;
import com.huawei.timeclaims.timeclaims.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin()
@RestController
public class ProjectController {

    private ProjectRepository projectRepository;
    private MemberRepository memberRepository;

    @Autowired
    public ProjectController(ProjectRepository projectRepository, MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
        this.projectRepository = projectRepository;
    }

    @GetMapping("/projects")
    public List<Project> getProjects() {
        return projectRepository.findProjectsQuery();
    }

    @GetMapping("/projects/{projectId}")
    public Optional<Project> getProjectById(@PathVariable Long projectId) {
        return projectRepository.findById(projectId);
    }

    @PostMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
    public Project createProject(@Valid @RequestBody Project project) {
        Long trackerId = project.getTracker().getId();
        Optional<Member> opt = memberRepository.findById(trackerId);
        Member member = opt.get();

        project.setTracker(member);
        return projectRepository.save(project);

    }

    @PutMapping("/projects/{projectId}")
    public Project updateProject(@PathVariable Long projectId, @Valid @RequestBody Project p) {
        return projectRepository.findById(projectId).map(project -> {
            project.setProjectName(p.getProjectName());
            project.setSiteName(p.getSiteName());
            project.setProjectLevel(p.getProjectLevel());
            project.setProjectType(p.getProjectType());
            project.setApprover(p.getApprover());

            Long trackerId = p.getTracker().getId();
            Optional<Member> opt = memberRepository.findById(trackerId);
            Member member = opt.get();
            project.setTracker(member);

            project.setProjectCode(p.getProjectCode());
            project.setSubProjectCode(p.getSubProjectCode());

            return projectRepository.save(project);
        }).orElseThrow(() -> new ResourceNotFoundException(("Project not found with id:" + p.getId())));
    }

    @PutMapping("/projects/delete/{projectId}")
    public Project deleteProject(@PathVariable Long projectId) {
        return projectRepository.findById(projectId).map(project -> {
            project.setActive(false);
            return projectRepository.save(project);
        }).orElseThrow(() -> new ResourceNotFoundException(("Project not found by id")));
    }

}