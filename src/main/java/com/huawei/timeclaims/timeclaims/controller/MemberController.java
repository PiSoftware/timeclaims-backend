package com.huawei.timeclaims.timeclaims.controller;


import com.huawei.timeclaims.timeclaims.exception.ResourceNotFoundException;
import com.huawei.timeclaims.timeclaims.model.Member;
import com.huawei.timeclaims.timeclaims.repository.MemberRepository;
import com.huawei.timeclaims.timeclaims.utils.CustomJSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@CrossOrigin()
@RestController
public class MemberController {

    private MemberRepository memberRepository;

    @Autowired
    public MemberController(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }

    @GetMapping(value = "/members")
    public List<Member> getMembers() {
        return memberRepository.findAllByActiveOrderByFullName(true);
    }

    @GetMapping("/members/{memberId}")
    public Optional<Member> getMemberById(@PathVariable Long memberId) {
        return memberRepository.findById(memberId);
    }

    @PostMapping(value = "/members", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createMembers(@Valid @RequestBody Member member) {
        try {
            memberRepository.save(member);
            return new ResponseEntity<>(CustomJSON.response("success","Added", "Saved Successfully."), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(CustomJSON.response("warning","Error", ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/members/{memberId}")
    public ResponseEntity<String> updateMember(@PathVariable Long memberId, @Valid @RequestBody Member m) {
        memberRepository.findById(memberId).map(member -> {
            member.setFullName(m.getFullName());
            member.setTeam(m.getTeam());
            memberRepository.save(member);
            return new ResponseEntity<>(CustomJSON.response("success","Added", "Saved Successfully."), HttpStatus.OK);
        }).orElseThrow(() -> new ResourceNotFoundException("Member not found with id:  " + m.getId()));

        return new ResponseEntity<>(CustomJSON.response("success","Updated", "Updated Successfully."), HttpStatus.OK);
    }

    @PutMapping("/members/delete/{memberId}")
    public ResponseEntity<String> deleteMember(@PathVariable Long memberId) {
        memberRepository.findById(memberId).map(member -> {
            member.setActive(false);
            return memberRepository.save(member);
        }).orElseThrow(() -> new ResourceNotFoundException("Member not found with id"));

        return new ResponseEntity<>(CustomJSON.response("success","Deleted", "Deleted Successfully."), HttpStatus.OK);

    }

}