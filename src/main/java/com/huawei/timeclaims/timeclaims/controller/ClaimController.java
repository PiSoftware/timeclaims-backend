package com.huawei.timeclaims.timeclaims.controller;

import com.alibaba.fastjson.JSONObject;

import com.huawei.timeclaims.timeclaims.exception.ResourceNotFoundException;
import com.huawei.timeclaims.timeclaims.model.Claim;
import com.huawei.timeclaims.timeclaims.model.Member;
import com.huawei.timeclaims.timeclaims.model.Project;
import com.huawei.timeclaims.timeclaims.model.Status;
import com.huawei.timeclaims.timeclaims.repository.*;
import com.huawei.timeclaims.timeclaims.utils.CustomJSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.text.DecimalFormat;
import java.util.*;

import static com.huawei.timeclaims.timeclaims.utils.DateUtils.getDateFromWeek;
import static com.huawei.timeclaims.timeclaims.utils.JSONUtils.getArrayFromJson;
import static com.huawei.timeclaims.timeclaims.utils.JSONUtils.getJsonObjectFromKey;
import static com.huawei.timeclaims.timeclaims.utils.JSONUtils.getJsonValueFromKey;
import static java.lang.Math.toIntExact;

@CrossOrigin()
@RestController
public class ClaimController {

    private ClaimRepository claimRepository;
    private ClaimLogsRepository claimLogsRepository;
    private StatusRepository statusRepository;
    private MemberRepository memberRepository;
    private ProjectRepository projectRepository;

    @Autowired
    public ClaimController(ClaimRepository claimRepository, StatusRepository statusRepository, MemberRepository memberRepository, ProjectRepository projectRepository, ClaimLogsRepository claimLogsRepository) {
        this.claimRepository = claimRepository;
        this.statusRepository = statusRepository;
        this.memberRepository = memberRepository;
        this.projectRepository = projectRepository;
        this.claimLogsRepository = claimLogsRepository;
    }

    @GetMapping("/claims")
    public List<Claim> getClaims() {
        return claimRepository.findAllByOrderByWeekDesc();
    }

    @GetMapping("/all")
    public JSONObject getAll() {
        JSONObject data = new JSONObject();
        JSONObject json = new JSONObject();
        data.put("claims", claimRepository.findAllByOrderByWeekDesc());
        data.put("members", memberRepository.findAllByActiveOrderByFullName(true));
        data.put("projects", projectRepository.findProjectsQuery());
        data.put("imported", claimLogsRepository.findAllByOrderByIdDesc());
        //data.put("distinctProjects", projectRepository.findDistinctProjectNames());
        json.put("data", data);
        json.put("message", "Retrieved data succesfully");
        return json;
    }

    @GetMapping("/test")
    public List<?> test() {
        return claimRepository.test();
    }

    @GetMapping("/claims/{claimId}")
    public Optional<Claim> getClaimsById(@PathVariable Long claimId) {
        return claimRepository.findById(claimId);
    }

    @GetMapping("/claims/{week}/{code}/{id}")
    public boolean testClaimCheck(@PathVariable Integer week, @PathVariable String code, @PathVariable Integer id) {
        return claimRepository.checkExistClaim(week, id, code);
    }

    @PostMapping("/claims")
    public ResponseEntity<String> createClaim(@Valid @RequestBody String jsonString) {
        String week = getJsonValueFromKey(jsonString, "week");
        String date = getJsonValueFromKey(jsonString, "date");

        Object statusJsonObject = getJsonObjectFromKey(jsonString, "status");
        String convertedToString = String.valueOf(statusJsonObject);

        String phm = getJsonValueFromKey(convertedToString, "phm");
        String pmd = getJsonValueFromKey(convertedToString, "pmd");
        //String amh = getJsonValueFromKey(convertedToString, "amh");
        String transferBy = getJsonValueFromKey(convertedToString, "transferBy");
        String status = getJsonValueFromKey(convertedToString, "status");

        Object str = getJsonObjectFromKey(convertedToString, "tracker");
        String trackerJsonString = String.valueOf(str);
        String trackerId = getJsonValueFromKey(trackerJsonString, "id");
        String remark = getJsonValueFromKey(convertedToString, "remark");

        Long l = Long.valueOf(trackerId);

        Optional<Member> opt = memberRepository.findById(l);
        Member tracker = opt.get();

        int phm_ = Integer.parseInt(phm);
        int pmd_ = Integer.parseInt(pmd);
        Status statusObject = new Status(Integer.parseInt(phm), Integer.parseInt(pmd), phm_ * pmd_,
                transferBy, status, remark);
        statusObject.setTracker(tracker);


        Object memberJsonObject = getJsonObjectFromKey(jsonString, "member");
        String memberToString = String.valueOf(memberJsonObject);
        String memberId = getJsonValueFromKey(memberToString, "id");
        System.out.print(memberId);
        Long memberIdLong = Long.valueOf(memberId);
        Optional<Member> opt2 = memberRepository.findById(memberIdLong);

        Object projectJsonObject = getJsonObjectFromKey(jsonString, "project");
        String projectToString = String.valueOf(projectJsonObject);
        String projectId = getJsonValueFromKey(projectToString, "id");
        Long projectIdLong = Long.valueOf(projectId);
        Optional<Project> opt3 = projectRepository.findById(projectIdLong);

        statusRepository.save(statusObject);
        Member member = opt2.get();
        Project project = opt3.get();

        Claim claim = new Claim(Integer.parseInt(week), date, member, project, statusObject);
        try {
            claimRepository.save(claim);
            return new ResponseEntity<>(CustomJSON.response("success","Added", "Created Successfully."), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(CustomJSON.response("error","Warning", ex.getMessage()), HttpStatus.OK);
        }
    }

    @PostMapping("/claims/*")
    public ResponseEntity<String> createMultipleClaims(@Valid @RequestBody String jsonString) {
        List<String> weeks = new ArrayList<String>(); //
        try {
            weeks = getArrayFromJson(jsonString, "week"); //if there is week array
        } catch (java.lang.ClassCastException e) {
            weeks.add(getJsonValueFromKey(jsonString, "week")); //if there is only 1 week
        }

        Object statusJsonObject = getJsonObjectFromKey(jsonString, "status");
        String convertedToString = String.valueOf(statusJsonObject);

        String phm = getJsonValueFromKey(convertedToString, "phm");
        String pmd = getJsonValueFromKey(convertedToString, "pmd");
        //String amh = getJsonValueFromKey(convertedToString, "amh");
        String transferBy = getJsonValueFromKey(convertedToString, "transferBy");
        String status = getJsonValueFromKey(convertedToString, "status");

        Object str = getJsonObjectFromKey(convertedToString, "tracker");
        String trackerJsonString = String.valueOf(str);
        String trackerId = getJsonValueFromKey(trackerJsonString, "id");
        String remark = getJsonValueFromKey(convertedToString, "remark");

        Long l = Long.valueOf(trackerId);

        Optional<Member> opt = memberRepository.findById(l);
        Member tracker = opt.get();

        double phm_ = Double.parseDouble(phm);
        int pmd_ = Integer.parseInt(pmd);

        Object memberJsonObject = getJsonObjectFromKey(jsonString, "member");
        String memberToString = String.valueOf(memberJsonObject);
        List<String> members = getArrayFromJson(memberToString, "id");

        Object projectJsonObject = getJsonObjectFromKey(jsonString, "project");
        String projectToString = String.valueOf(projectJsonObject);
        String projectId = getJsonValueFromKey(projectToString, "id");
        Long projectIdLong = Long.valueOf(projectId);
        Optional<Project> optionalProject = projectRepository.findById(projectIdLong);

        Project project = optionalProject.get();

        for (String member1 : members) {
            Iterator<String> iteratorWeeks = weeks.iterator();
            String id = member1;
            Long memberIdLong = Long.valueOf(id);
            Optional<Member> optionalMember = memberRepository.findById(memberIdLong);
            Member member = optionalMember.get();
            DecimalFormat df2 = new DecimalFormat(".##");
            //String numberAsString = String.format ("%.2f", phm_*pmd_);
            while (iteratorWeeks.hasNext()) {
                String strWeek = iteratorWeeks.next();
                boolean isClaimExists = claimRepository.checkExistClaim(Integer.parseInt(strWeek), toIntExact(member.getId()), project.getProjectCode());
                if(isClaimExists){
                    return new ResponseEntity<>(CustomJSON.response("error","Warning", "This claim exists. (Week,Member,Project)"), HttpStatus.OK);
                }
                Status statusObject = new Status(Double.parseDouble(phm), Integer.parseInt(pmd),
                        Double.valueOf(df2.format(phm_ * pmd_).replace(",", ".")),
                        transferBy, status, remark);
                statusObject.setTracker(tracker);
              statusRepository.save(statusObject);

                String date = getDateFromWeek(Integer.parseInt(strWeek)); //Integer.parseInt(strWeek), 2018
                Claim claim = new Claim(Integer.parseInt(strWeek), date, member, project, statusObject);
                claimRepository.save(claim);
            }
        }

        return new ResponseEntity<>(CustomJSON.response("success","Added", "Saved Successfully."), HttpStatus.OK);

    }

    @DeleteMapping("/claims/{claimId}")
    public ResponseEntity<?> deleteClaim(@PathVariable Long claimId) {
        claimRepository.deleteById(claimId);
        return new ResponseEntity<>(CustomJSON.response("success","Deleted", "Deleted Successfully."), HttpStatus.OK);
    }

    @PutMapping("/claims/{claimId}/status/{statusId}")
    public Claim updateClaim(@PathVariable Long claimId, @PathVariable Long statusId, @Valid @RequestBody String jsonString) {
        String week = getJsonValueFromKey(jsonString, "week");
        String date = getJsonValueFromKey(jsonString, "date");

        Object statusJsonObject = getJsonObjectFromKey(jsonString, "status");
        String convertedToString = String.valueOf(statusJsonObject);

        String phm = getJsonValueFromKey(convertedToString, "phm");
        String pmd = getJsonValueFromKey(convertedToString, "pmd");
        //String amh = getJsonValueFromKey(convertedToString, "amh");
        String transferBy = getJsonValueFromKey(convertedToString, "transferBy");
        String status = getJsonValueFromKey(convertedToString, "status");

        Object str = getJsonObjectFromKey(convertedToString, "tracker");
        String trackerJsonString = String.valueOf(str);
        String trackerId = getJsonValueFromKey(trackerJsonString, "id");
        String remark = getJsonValueFromKey(convertedToString, "remark");

        Long l = Long.valueOf(trackerId);

        Optional<Member> opt = memberRepository.findById(l);
        Member tracker = opt.get();


        Object memberJsonObject = getJsonObjectFromKey(jsonString, "member");
        String memberToString = String.valueOf(memberJsonObject);
        String memberId = getJsonValueFromKey(memberToString, "id");
        Long memberIdLong = Long.valueOf(memberId);
        Optional<Member> opt2 = memberRepository.findById(memberIdLong);

        Object projectJsonObject = getJsonObjectFromKey(jsonString, "project");
        String projectToString = String.valueOf(projectJsonObject);
        String projectId = getJsonValueFromKey(projectToString, "id");
        Long projectIdLong = Long.valueOf(projectId);
        Optional<Project> opt3 = projectRepository.findById(projectIdLong);

        Member member = opt2.get();
        Project project = opt3.get();

        double phm_ = Double.parseDouble(phm);
        int pmd_ = Integer.parseInt(pmd);

        DecimalFormat df2 = new DecimalFormat(".##");

        statusRepository.findById(statusId).map(status1 -> {
            status1.setTransferBy(transferBy);
            status1.setRemark(remark);
            status1.setAmh(Double.valueOf(df2.format(phm_ * pmd_).replace(",", ".")));
            status1.setPhm(Double.parseDouble(phm));
            status1.setPmd(Integer.parseInt(pmd));
            status1.setStatus(status);
            status1.setTracker(tracker);
            //set each value
            return statusRepository.save(status1);
        }).orElseThrow(() -> new ResourceNotFoundException("Not Found " + statusId));

        return claimRepository.findById(claimId).map(claim -> {
            claim.setWeek(Integer.parseInt((week.replaceAll("[^\\d.]", ""))));
            claim.setDate(getDateFromWeek(Integer.parseInt(week.replaceAll("[^\\d.]", ""))));
            claim.setMember(member);
            claim.setProject(project);
            return claimRepository.save(claim);
        }).orElseThrow(() -> new NullPointerException(""));

    }


}