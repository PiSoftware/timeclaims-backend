package com.huawei.timeclaims.timeclaims.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NumberUtils {
    public static boolean isNumeric(String s) {
        return s != null && s.matches("[-+]?\\d*\\.?\\d+");
    }

    public static int weekOfYear(String stringDate) {
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(stringDate);
            String a = new SimpleDateFormat("w").format(date);
            return Integer.parseInt(a);
        } catch (ParseException e) {
            return 0;
        }

    }

}