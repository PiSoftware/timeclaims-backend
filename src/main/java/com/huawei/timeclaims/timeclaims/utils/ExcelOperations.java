package com.huawei.timeclaims.timeclaims.utils;


import com.huawei.timeclaims.timeclaims.model.*;
import com.huawei.timeclaims.timeclaims.repository.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ExcelOperations {

    static final String EMPTY_PROJECT = "leave-GTS";
    static final String DEFAULT_CLAIMER_DEPT = "Turkey R&D SmartCare DC";
    static final Long DEFAULT_CLAIM_TRACKER_ID = 741601L;
    static final Long DEFAULT_PROJECT_TRACKER_ID = 741601L;
    static final String DEFAULT_TEAM = "Smartcare";
    static final String DEFAULT_SITENAME = "Smartcare";

    static final int COLUMN_WORKWEEK = 0;
    static final int COLUMN_CLAIM_PERIOD = 1;
    static final int COLUMN_CLAIMER_EMPLOYEE_ID = 2;
    static final int COLUMN_CLAIMER = 3;
    static final int COLUMN_CLAIMER_DEPT_CODE = 4;
    static final int COLUMN_CLAIMER_DEPT = 5;
    static final int COLUMN_APPROVED_DATE = 6;
    static final int COLUMN_PROJECT_CODE = 7;
    static final int COLUMN_PROJECT_NAME = 8;
    static final int COLUMN_ACTIVITY_CODE = 9;
    static final int COLUMN_ACTIVITY_NAME = 10;
    static final int COLUMN_WORK_HOURS = 11;


    private MemberRepository memberRepository;
    private ProjectRepository projectRepository;
    private ClaimRepository claimRepository;
    private StatusRepository statusRepository;
    private ClaimLogsRepository claimLogsRepository;
    private StatusLimboRepository statusLimboRepository;

    @Autowired
    public ExcelOperations(MemberRepository memberRepository, ProjectRepository projectRepository, ClaimRepository claimRepository, StatusRepository statusRepository, ClaimLogsRepository claimLogsRepository, StatusLimboRepository statusLimboRepository) {
        this.memberRepository = memberRepository;
        this.projectRepository = projectRepository;
        this.claimRepository = claimRepository;
        this.statusRepository = statusRepository;
        this.claimLogsRepository = claimLogsRepository;
        this.statusLimboRepository=statusLimboRepository;
    }

    public void exportExcel(String exportName, HttpServletResponse response) {
        List<Claim> claims = claimRepository.findAllByOrderByWeekDesc();

        Workbook workbook = new XSSFWorkbook();

        Sheet sheet = workbook.createSheet("TimeClaims");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 11);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        headerCellStyle.setFillBackgroundColor(IndexedColors.BLUE.getIndex());


        Row headerRow = sheet.createRow(0);

        String[] columns = {"Work Week", "Date ", "Claimer ID", "Claimer", "Team", "Site Name", "Project Name", "Project Code", "PHM", "PMD", "AMH", "Status"};
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }


        int rowNum = 1;
        for (Claim claim : claims) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue("W" + claim.getWeek());
            row.createCell(1).setCellValue(claim.getDate());
            row.createCell(2).setCellValue(claim.getMember().getId());
            row.createCell(3).setCellValue(claim.getMember().getFullName());
            row.createCell(4).setCellValue(claim.getMember().getTeam());
            row.createCell(5).setCellValue(claim.getProject().getSiteName());
            row.createCell(6).setCellValue(claim.getProject().getProjectName());
            row.createCell(7).setCellValue(claim.getProject().getProjectCode());
            row.createCell(8).setCellValue(claim.getStatus().getPhm());
            row.createCell(9).setCellValue(claim.getStatus().getPmd());
            row.createCell(10).setCellValue(claim.getStatus().getAmh());
            row.createCell(11).setCellValue(claim.getStatus().getStatus());
        }


        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }
        try {
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + exportName + "\"");
            workbook.write(response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Claim> importExcel(String filename) throws Exception {
        Optional<Member> defaultProjectTracker = memberRepository.findById(DEFAULT_PROJECT_TRACKER_ID);
        Optional<Member> defaultClaimTracker = memberRepository.findById(DEFAULT_CLAIM_TRACKER_ID);
        if (!defaultClaimTracker.isPresent() || !defaultProjectTracker.isPresent()) {
            throw new Exception("Default Project Tracker / Claim Tracker not found !");
        }


        List<Claim> importedClaims = new ArrayList<Claim>();
        boolean isStacked = false;

        Workbook workbook = null;
        try {
            try {
                workbook = WorkbookFactory.create(new File("./uploads/" + filename));
            } catch (Exception e) {
                throw e;
            }

            Sheet sheet = workbook.getSheetAt(0);
            DataFormatter dataFormatter = new DataFormatter();

            for (Row row : sheet) {
                if (isFirstRow(row, sheet)) continue;
                if (rowSkipProcedures(row)) continue;

                int workWeek = 0;
                long memberId = 0;
                Claim claim = new Claim();
                for (Cell cell : row) {
                    int columnIndex = cell.getColumnIndex();
                    String cellValue = dataFormatter.formatCellValue(cell);

                    switch (columnIndex) {
                        case COLUMN_WORKWEEK: //Work Week
                            workWeek = NumberUtils.weekOfYear(dataFormatter.formatCellValue(row.getCell(COLUMN_WORKWEEK)));
                            String date = cellValue.replace("-", ".").replace("~", " - ");
                            claim.setDate(date);
                            claim.setWeek(workWeek);
                            break;
                        case COLUMN_CLAIM_PERIOD: //Claim Period
                            String claimPeriod = dataFormatter.formatCellValue(row.getCell(COLUMN_CLAIM_PERIOD));
//                            claim.setWeek(NumberUtils.weekOfYear(claimPeriod));
                            break;
                        case COLUMN_CLAIMER_EMPLOYEE_ID: // Claimer Employee ID
                            if (!NumberUtils.isNumeric(cellValue)) {
                                continue;
                            }
                            Optional<Member> member = memberRepository.findById(Long.parseLong(cellValue));
                            if (member.isPresent()) {
                                claim.setMember(member.get());
                                memberId = member.get().getId();
                            } else {
                                Member newMember = new Member();
                                String claimerName = row.getCell(COLUMN_CLAIMER).getStringCellValue();
                                //String team = row.getCell(10).getStringCellValue();
                                newMember.setActive(true);
                                newMember.setId(Long.parseLong(cellValue));
                                newMember.setFullName(claimerName);
                                newMember.setTeam(DEFAULT_TEAM);
                                memberRepository.save(newMember);
                                claim.setMember(newMember);
                                memberId = newMember.getId();
                            }

                            break;

                        case COLUMN_PROJECT_NAME: //Project Name
                            Project project = projectRepository.findByProjectName(dataFormatter.formatCellValue(row.getCell(COLUMN_PROJECT_NAME)));
                            if (project == null) {
                                Project newProject = new Project();
                                newProject.setActive(true);
                                newProject.setSiteName(DEFAULT_SITENAME);
                                newProject.setProjectName(dataFormatter.formatCellValue(row.getCell(COLUMN_PROJECT_NAME)));
                                newProject.setProjectCode(dataFormatter.formatCellValue(row.getCell(COLUMN_PROJECT_CODE)));
                                defaultProjectTracker.ifPresent(newProject::setTracker);
                                projectRepository.save(newProject);
                                claim.setProject(newProject);

                            } else {
                                claim.setProject(project);
                            }
                            break;
                    }
                }

                boolean isClaimExists = claimRepository.checkExistClaim(workWeek, Integer.parseInt(String.valueOf(memberId)), dataFormatter.formatCellValue(row.getCell(COLUMN_PROJECT_CODE)));

                Integer workHour = (int) row.getCell(COLUMN_WORK_HOURS).getNumericCellValue();
                Integer workDay = 1;
                String transferBy = "times";
                String remark = row.getCell(COLUMN_ACTIVITY_NAME).getStringCellValue();

                String statusStatus;
                try {
                    statusStatus = row.getCell(COLUMN_APPROVED_DATE).getStringCellValue().equals("") ? "Claimed" : "Approved";
                } catch (Exception e) {
                    statusStatus = "Claimed";
                }
                Status newStatus = new Status(workHour, workDay, workDay * workHour, transferBy, statusStatus, remark);
                defaultClaimTracker.ifPresent(newStatus::setTracker);
                if (!isClaimExists) {
                    statusRepository.save(newStatus);
                }
                claim.setStatus(newStatus);

                System.out.println(claim.toString());

                isStacked = false;
                for (Claim c : importedClaims) {
                    if (c.getWeek() == claim.getWeek() && c.getMember().getId() == claim.getMember().getId() && c.getProject().getProjectCode().equals(claim.getProject().getProjectCode())) {
                        c.getStatus().setPmd(c.getStatus().getPmd() + claim.getStatus().getPmd());
                        //c.getStatus().setPhm(c.getStatus().getPhm() + claim.getStatus().getPhm());
                        c.getStatus().setAmh(c.getStatus().getAmh() + claim.getStatus().getAmh());
                        //c.setImportStatus(ClaimLogs.UPDATE_AMH);
                        statusRepository.save(c.getStatus());
                        claimRepository.save(c);
                        claimLogsRepository.save(c.toClaimLogs(ClaimLogs.UPDATE_AMH));
                        isStacked = true;
                        break;
                    }
                }
                if (!isStacked) {
                    importedClaims.add(claim);
                    if (!isClaimExists) {
                        claimRepository.save(claim);
                        claimLogsRepository.save(claim.toClaimLogs(ClaimLogs.NEW));
                    }
                }
            }


        } finally {
            if (workbook != null) {
                workbook.close();

            }

        }
        return importedClaims;
    }


    private boolean rowSkipProcedures(Row row) {
        String projectName = row.getCell(COLUMN_PROJECT_NAME).getStringCellValue();
        String claimerDept = row.getCell(COLUMN_CLAIMER_DEPT).getStringCellValue();
        if (projectName.equals(ExcelOperations.EMPTY_PROJECT) || projectName.equals("")) {
            return true;
        }
        return !claimerDept.equals(ExcelOperations.DEFAULT_CLAIMER_DEPT);
    }

    private boolean isFirstRow(Row row, Sheet sheet) {
        return sheet.getRow(0) == row;
    }

    private Object getCellValue(Cell cell) {
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                return cell.getStringCellValue();

            case Cell.CELL_TYPE_BOOLEAN:
                return cell.getBooleanCellValue();

            case Cell.CELL_TYPE_NUMERIC:
                return cell.getNumericCellValue();
        }

        return null;
    }
}