package com.huawei.timeclaims.timeclaims.utils;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;

public class CustomJSON {
    public static String json(String key,String message) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(key,message);
        return new Gson().toJson(jsonObject);
    }
    public static String response(String type,String title,String message) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type",type);
        jsonObject.put("title",title);
        jsonObject.put("message",message);
        return new Gson().toJson(jsonObject);
    }
}
