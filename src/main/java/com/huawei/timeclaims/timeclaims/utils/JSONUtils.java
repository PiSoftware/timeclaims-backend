package com.huawei.timeclaims.timeclaims.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

public class JSONUtils {
    public static String getJsonValueFromKey(String jsonString, String key){
        JsonParser jsonParser = new JsonParser();
        JsonElement jsonElement = jsonParser.parse(jsonString);
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        jsonObject = jsonObject.getAsJsonObject();

        String result = jsonObject.get(key).getAsString();

        return result;
    }

    public static List<String> getArrayFromJson(String jsonString, String key){
        JsonParser jsonParser = new JsonParser();
        JsonElement jsonElement = jsonParser.parse(jsonString);
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        jsonObject = jsonObject.getAsJsonObject();

        JsonArray result = jsonObject.getAsJsonArray(key);

        List<String> returnList = new ArrayList<String>();

        for (int i = 0; i < result.size(); i++) {
            String str = (result.get(i).getAsString());
            returnList.add(str);
        }
        return returnList;
    }

    public static Object getJsonObjectFromKey(String jsonString, String key){
        JsonParser jsonParser = new JsonParser();
        JsonElement jsonElement = jsonParser.parse(jsonString);
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        jsonObject = jsonObject.getAsJsonObject();

        Object result = jsonObject.get(key);

        return result;
    }
}
