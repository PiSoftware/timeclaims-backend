package com.huawei.timeclaims.timeclaims.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateUtils {
    public static String getDateFromWeek(int week){
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.WEEK_OF_YEAR, week);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        String startDate = (sdf.format(cal.getTime()));

        cal.add(Calendar.DATE, 6);
        String endDate = (sdf.format(cal.getTime()));
        return startDate + " - " + endDate;
    }
}
